VERSION 5.00
Begin VB.UserControl tucaoSHOW 
   BackColor       =   &H8000000B&
   ClientHeight    =   1770
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2100
   ScaleHeight     =   1770
   ScaleWidth      =   2100
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "NicoPoiDuang~"
      BeginProperty Font 
         Name            =   "微软雅黑"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Image Image1 
      Height          =   1095
      Left            =   120
      Stretch         =   -1  'True
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "tucaoSHOW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'事件声明:
Event Click() 'MappingInfo=UserControl,UserControl,-1,Click
Event DblClick() 'MappingInfo=UserControl,UserControl,-1,DblClick
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=UserControl,UserControl,-1,MouseDown
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=UserControl,UserControl,-1,MouseMove
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=UserControl,UserControl,-1,MouseUp
'
'注意！不要删除或修改下列被注释的行！
'MappingInfo=UserControl,UserControl,-1,BackColor
Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As OLE_COLOR)
    UserControl.BackColor() = New_BackColor
    PropertyChanged "BackColor"
End Property

Private Sub Image1_Click()
RaiseEvent Click
End Sub

Private Sub Label1_Click()
RaiseEvent Click
End Sub

Private Sub Label1_DblClick()
RaiseEvent DblClick
End Sub

Private Sub UserControl_Click()
    RaiseEvent Click
End Sub

Private Sub UserControl_DblClick()
    RaiseEvent DblClick
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub label1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub label1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub
'注意！不要删除或修改下列被注释的行！
'MappingInfo=Label1,Label1,-1,Caption
Public Property Get Caption() As String
Attribute Caption.VB_Description = "返回/设置对象的标题栏中或图标下面的文本。"
    Caption = Label1.Caption
End Property

Public Property Let Caption(ByVal New_Caption As String)
    If Len(New_Caption) > 21 Then
    tmp1 = Mid(New_Caption, 1, 10)
    tmp2 = Mid(New_Caption, Len(New_Caption) - 10)
    New_Caption = tmp1 & "……" & tmp2
    End If
    Label1.Caption() = New_Caption
    PropertyChanged "Caption"
End Property

'从存贮器中加载属性值
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    UserControl.BackColor = PropBag.ReadProperty("BackColor", &H8000000F)
    Label1.Caption = PropBag.ReadProperty("Caption", "蚊子没节操蚊子没节操")
    Label1.ToolTipText = PropBag.ReadProperty("URL", "")
    Set Picture = PropBag.ReadProperty("Picture", Nothing)
End Sub

Private Sub UserControl_Resize()
UserControl.Height = 1770
UserControl.Width = 2100
End Sub

'将属性值写到存储器
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

    Call PropBag.WriteProperty("BackColor", UserControl.BackColor, &H8000000F)
    Call PropBag.WriteProperty("Caption", Label1.Caption, "蚊子没节操蚊子没节操")
    Call PropBag.WriteProperty("URL", Label1.ToolTipText, "")
    Call PropBag.WriteProperty("Picture", Picture, Nothing)
End Sub

'注意！不要删除或修改下列被注释的行！
'MappingInfo=Label1,Label1,-1,ToolTipText
Public Property Get URL() As String
    URL = Label1.ToolTipText
End Property

Public Property Let URL(ByVal New_URL As String)
    Label1.ToolTipText() = New_URL
    PropertyChanged "URL"
End Property

'注意！不要删除或修改下列被注释的行！
'MappingInfo=UserControl,UserControl,-1,Picture
Public Property Get Picture() As Picture
Attribute Picture.VB_Description = "返回/设置控件中显示的图形。"
    Set Picture = UserControl.Picture
End Property

Public Property Set Picture(ByVal New_Picture As Picture)
    Set UserControl.Picture = New_Picture
    PropertyChanged "Picture"
End Property

'注意！不要删除或修改下列被注释的行！
'MemberInfo=14
Public Function SetPictureFromPath(ByVal PicturePath As String) As Variant
On Error Resume Next
Image1.Picture = LoadPicture(PicturePath)
End Function

