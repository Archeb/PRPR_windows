VERSION 5.00
Begin VB.UserControl PUIMgr 
   ClientHeight    =   750
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3300
   InvisibleAtRuntime=   -1  'True
   ScaleHeight     =   750
   ScaleWidth      =   3300
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   480
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   0
      Top             =   0
   End
End
Attribute VB_Name = "PUIMgr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Event MoveSmlyComplete(Control As Object)
Public Event SizeSmlyComplete(Control As Object)
'��������������������������������������������������������������������������������������������������������������������������
Dim C1 As Object
Dim gLeft As Integer
Dim gTop As Integer
'��������������������������������������������������������������������������������������������������������������������������
Dim C2 As Object
Dim gWidth As Integer
Dim gHeight As Integer
'��������������������������������������������������������������������������������������������������������������������������
Private Sub Timer1_Timer()
    If Abs(C1.Left - gLeft) > 15 Then
        C1.Left = C1.Left - (C1.Left - gLeft) / 10
    Else
        C1.Left = gLeft
    End If
    If Abs(C1.Top - gTop) > 15 Then
        C1.Top = C1.Top - (C1.Top - gTop) / 10
    Else
        C1.Top = gTop
    End If
    If (C1.Left = gLeft) And (C1.Top = gTop) Then
        RaiseEvent MoveSmlyComplete(C1)
        Timer1.Enabled = False
    End If
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Private Sub Timer2_Timer()
    If Abs(C2.Width - gWidth) > 15 Then
        C2.Width = C2.Width - (C2.Width - gWidth) / 10
    Else
        C2.Width = gWidth
    End If
    If Abs(C2.Height - gHeight) > 15 Then
        C2.Height = C2.Height - (C2.Height - gHeight) / 10
    Else
        C2.Height = gHeight
    End If
    If (C2.Width = gWidth) And (C2.Height = gHeight) Then
        RaiseEvent SizeSmlyComplete(C2)
        Timer2.Enabled = False
    End If
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Private Sub UserControl_Resize()
    Width = 480
    Height = 480
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Public Sub MoveSmly(ByRef Control As Object, ByVal nLeft As Integer, ByVal nTop As Integer, ByVal Delay As Integer)
    If (Delay > 1000) Or (Delay < 1) Then Exit Sub
    Timer1.Interval = Delay
    gLeft = nLeft
    gTop = nTop
    Set C1 = Control
    Timer1.Enabled = True
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Public Sub SizeSmly(ByRef Control As Object, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal Delay As Integer)
    If (Delay > 1000) Or (Delay < 1) Then Exit Sub
    Timer2.Interval = Delay
    gWidth = nWidth
    gHeight = nHeight
    Set C2 = Control
    Timer2.Enabled = True
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Public Sub ControlTransparent(ByRef Container As Object, ByRef Control As Object, ByVal Transparency As Integer)
    If (Transparency < 0) Or (Transparency > 255) Then Exit Sub
    Container.AutoRedraw = True
    Control.AutoRedraw = True
    Control.BackColor = Control.BackColor
    Dim bf As BLENDFUNCTION, IBF As Long
    With bf
        .BlendOp = AC_SRC_OVER
        .BlendFlags = 0
        .SourceConstantAlpha = Transparency
        .AlphaFormat = 0
    End With
    RtlMoveMemory IBF, bf, 4
    AlphaBlend Control.hDC, 0, 0, Control.ScaleWidth / 15, Control.ScaleHeight / 15, Container.hDC, Control.Left / 15, Control.Top / 15, Control.ScaleWidth / 15, Control.ScaleHeight / 15, IBF
End Sub
'��������������������������������������������������������������������������������������������������������������������������
Public Sub FormTransparent(ByRef Frm As Form, ByVal Transparency As Integer)
    Dim R As Long
    R = GetWindowLong(Frm.hWnd, GWL_EXSTYLE)
    R = R Or WS_EX_LAYERED
    SetWindowLong Frm.hWnd, GWL_EXSTYLE, R
    SetLayeredWindowAttributes Frm.hWnd, 0, Transparency, LWA_ALPHA
End Sub
