Attribute VB_Name = "APIs"
Public Declare Function SetWindowPos& Lib "user32" (ByVal hwnd As Long, _
    ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
    ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
Public Declare Sub sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


  Public Declare Function ReleaseCapture Lib "user32" () As Long
  Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
  Public Const HTCAPTION = 2
  Public Const WM_NCLBUTTONDOWN = &HA1
Public Declare Function SleepEx Lib "kernel32" (ByVal dwMilliseconds As Long, ByVal bAlertable As Long) As Long

Public Declare Function URLDownloadToFile Lib "urlmon" Alias "URLDownloadToFileA" (ByVal pCaller As Long, ByVal szURL As String, ByVal szFileName As String, ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long
Public Const CS_DROPSHADOW = &H20000
Public Const GCL_STYLE = (-26)
    
Public Declare Function GetClassLong Lib "user32" Alias "GetClassLongA" ( _
          ByVal hwnd As Long, _
          ByVal nIndex As Long) As Long
Public Declare Function SetClassLong Lib "user32" Alias "SetClassLongA" ( _
          ByVal hwnd As Long, _
          ByVal nIndex As Long, _
          ByVal dwNewLong As Long) As Long
    
Public Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Public Declare Function CreateRoundRectRgn Lib "gdi32" (ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, ByVal Y3 As Long) As Long
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
'我们先来了解一下这几个函数
'函数CreateRoundRectRgn用于创建一个圆角矩形，该矩形由X1，Y1-X2，Y2确定，
'并由X3，Y3确定的椭圆描述圆角弧度
'CreateRoundRectRgn参数 类型及说明
'X1,Y1 Long，矩形左上角的X，Y坐标
'X2,Y2 Long，矩形右下角的X，Y坐标
'X3 Long，圆角椭圆的宽。其范围从0（没有圆角）到矩形宽（全圆）
'Y3 Long，圆角椭圆的高。其范围从0（没有圆角）到矩形高（全圆）
'SetWindowRgn用于将CreateRoundRectRgn创建的圆角区域赋给窗体
'DeleteObject用于将CreateRoundRectRgn创建的区域删除，这是必要的，否则不必要的占用电脑内存
'接下来声明一个全局变量,用来获得区域句柄，如下：
Public outrgn As Long
'然后分别在窗体Activate()事件和Unload事件中输入以下代码

'接下来我们开始编写子过程
Public Sub rgnform(ByVal frmbox As Form, ByVal fw As Long, ByVal fh As Long)
Dim w As Long, h As Long
w = frmbox.ScaleX(frmbox.Width, vbTwips, vbPixels)
h = frmbox.ScaleY(frmbox.Height, vbTwips, vbPixels)
outrgn = CreateRoundRectRgn(0, 0, w, h, fw, fh)
Call SetWindowRgn(frmbox.hwnd, outrgn, True)
End Sub
