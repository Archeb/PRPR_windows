VERSION 5.00
Begin VB.UserControl PTab 
   BackColor       =   &H00E1A400&
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ControlContainer=   -1  'True
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   ToolboxBitmap   =   "PTab.ctx":0000
   Begin PRPRProject.PUIMgr PM 
      Left            =   360
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
   End
   Begin VB.PictureBox p 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   80
      Left            =   120
      ScaleHeight     =   75
      ScaleWidth      =   375
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label l 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      Height          =   180
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   540
   End
End
Attribute VB_Name = "PTab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Dim C_Color_Back As OLE_COLOR
Dim C_Color_Text As OLE_COLOR
Dim C_Picture As Picture
Dim C_Font_Name As String
Dim C_Font_Size As Integer
Dim C_Font_Bold As Boolean
Dim C_Font_Italic As Boolean
Dim C_Font_Underline As Boolean
Dim C_Enabled As Boolean
Dim C_Distance As Integer
Dim C_SelectedColor As OLE_COLOR
Dim C_Color_Selecter As OLE_COLOR

Private Type Containers
    Container As Object
    Text As String
End Type

Dim Items() As Containers
Dim Total As Integer
Dim Checked As Integer

Dim GoalLeft As Integer
Dim GoalWidth As Integer

Public Event Click()
Public Event DblClick()
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event ItemSelected(NewIndex As Integer)

Public Sub Refresh()
    Dim i As Integer
    For i = 1 To l.UBound
        Unload l(i)
    Next
    p.BackColor = C_Color_Selecter
    l(0).Left = 120
    l(0).Top = 120
    UserControl.BackColor = C_Color_Back
    l(0).ForeColor = C_Color_Text
    Set UserControl.Picture = C_Picture
    l(0).FontName = C_Font_Name
    l(0).FontSize = C_Font_Size
    l(0).FontBold = C_Font_Bold
    l(0).FontItalic = C_Font_Italic
    l(0).FontUnderline = C_Font_Underline
    If Total = -1 Then
        p.Visible = False
        l(0).Visible = False
        Checked = -1
        RaiseEvent ItemSelected(-1)
        Exit Sub
    ElseIf Total = 0 Then
        p.Visible = True
        l(0).Visible = True
        l(0) = Items(0).Text
        Checked = 0
        Items(0).Container.Visible = True
        l(0).ForeColor = C_SelectedColor
        RaiseEvent ItemSelected(0)
        Exit Sub
    End If
    PM.MoveSmly p, 120, l(0).Top + l(0).Height + 45, 10
    For i = 1 To Total
        Load l(i)
        Items(i).Container.Visible = False
        l(i).Top = l(i - 1).Top
        l(i).Left = l(i - 1).Left + l(i - 1).Width + C_Distance
        l(i) = Items(i).Text
        If l(i).Left > UserControl.Width - l(i).Width Then
            l(i).Top = l(i - 1).Top + l(i).Height + C_Distance + p.Height
            l(i).Left = 120
        End If
        l(i).ForeColor = C_Color_Text
        l(i).Visible = True
    Next
    l(0).ForeColor = C_SelectedColor
    Checked = 0
    L_Click 0
End Sub

Public Sub BeRelated(ByRef Container As Object, Text As String)
    If Total = 99 Then Exit Sub
    Total = Total + 1
    Set Items(Total).Container = Container
    Items(Total).Text = Text
    Refresh
End Sub

Public Sub BeUnRelated(Index As Integer)
    If (Index < 0) Or (Index > 99) Then Exit Sub
    Items(Index).Container.Visible = False
    Dim i As Integer
    For i = Index To Total - 1
        Items(i) = Items(i + 1)
    Next
    Total = Total - 1
    Refresh
End Sub

Public Sub Clear()
    ReDim Items(99)
    Total = -1
    Refresh
End Sub

Public Property Get Color_Back() As OLE_COLOR
    Color_Back = C_Color_Back
End Property

Public Property Let Color_Back(ByVal vNewValue As OLE_COLOR)
    C_Color_Back = vNewValue
    PropertyChanged "Color_Back"
    Refresh
End Property

Public Property Get Color_Text() As OLE_COLOR
    Color_Text = C_Color_Text
End Property

Public Property Let Color_Text(ByVal vNewValue As OLE_COLOR)
    C_Color_Text = vNewValue
    PropertyChanged "Color_Text"
    Refresh
End Property

Public Property Get Picture() As Picture
    Set Picture = C_Picture
End Property

Public Property Set Picture(ByVal vNewValue As Picture)
    Set C_Picture = vNewValue
    PropertyChanged "Picture"
    Refresh
End Property

Public Property Get Enabled() As Boolean
    Enabled = C_Enabled
End Property

Public Property Let Enabled(ByVal vNewValue As Boolean)
    C_Enabled = vNewValue
    PropertyChanged "Enabled"
End Property

Public Property Get Font_Name() As String
    Font_Name = C_Font_Name
End Property

Public Property Let Font_Name(ByVal vNewValue As String)
    C_Font_Name = vNewValue
    PropertyChanged "Font_Name"
    Refresh
End Property

Public Property Get Font_Size() As Integer
    Font_Size = C_Font_Size
End Property

Public Property Let Font_Size(ByVal vNewValue As Integer)
    If vNewValue <= 0 Then vNewValue = 1
    C_Font_Size = vNewValue
    PropertyChanged "Font_Size"
    Refresh
End Property

Public Property Get Font_Bold() As Boolean
    Font_Bold = C_Font_Bold
End Property

Public Property Let Font_Bold(ByVal vNewValue As Boolean)
    C_Font_Bold = vNewValue
    PropertyChanged "Font_Bold"
    Refresh
End Property

Public Property Get Font_Italic() As Boolean
    Font_Italic = C_Font_Italic
End Property

Public Property Let Font_Italic(ByVal vNewValue As Boolean)
    C_Font_Italic = vNewValue
    PropertyChanged "Font_Italic"
    Refresh
End Property

Public Property Get Font_Underline() As Boolean
    Font_Underline = C_Font_Underline
End Property

Public Property Let Font_Underline(ByVal vNewValue As Boolean)
    C_Font_Underline = vNewValue
    PropertyChanged "Font_Underline"
    Refresh
End Property

Public Property Get Distance() As Integer
    Distance = C_Distance
End Property

Public Property Let Distance(ByVal vNewValue As Integer)
    If vNewValue < 0 Then vNewValue = 0
    C_Distance = vNewValue
    PropertyChanged "Distance"
    Refresh
End Property

Public Property Get SelectedColor() As OLE_COLOR
    SelectedColor = C_SelectedColor
End Property

Public Property Let SelectedColor(ByVal vNewValue As OLE_COLOR)
    C_SelectedColor = vNewValue
    PropertyChanged "SelectedColor"
    Refresh
End Property

Public Property Get Color_Selecter() As OLE_COLOR
    Color_Selecter = C_Color_Selecter
End Property

Public Property Let Color_Selecter(ByVal vNewValue As OLE_COLOR)
    C_Color_Selecter = vNewValue
    PropertyChanged "Color_Selecter"
    Refresh
End Property

Private Sub L_Click(Index As Integer)
    Items(Checked).Container.Visible = False
    l(Checked).ForeColor = C_Color_Text
    Checked = Index
    Items(Checked).Container.Visible = True
    l(Checked).ForeColor = C_SelectedColor
    PM.MoveSmly p, l(Index).Left, l(Index).Top + l(Index).Height + 45, 10
    PM.SizeSmly p, l(Index).Width, p.Height, 10
    RaiseEvent ItemSelected(Index)
End Sub

Private Sub UserControl_Click()
    RaiseEvent Click
End Sub

Private Sub UserControl_DblClick()
    RaiseEvent DblClick
End Sub

Private Sub UserControl_Initialize()
    Total = -1
    ReDim Items(99)
    
    C_Color_Back = &HE1A400
    C_Color_Text = &H0&
    Set C_Picture = Nothing
    C_Font_Name = "΢���ź�"
    C_Font_Size = 10
    C_Font_Bold = False
    C_Font_Italic = False
    C_Font_Underline = False
    C_Enabled = True
    C_Distance = 240
    C_SelectedColor = &HFFFFFF
    Refresh
End Sub

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    C_Color_Back = PropBag.ReadProperty("Color_Back", &HE1A400)
    C_Color_Text = PropBag.ReadProperty("Color_Text", &H0&)
    Set C_Picture = PropBag.ReadProperty("Picture", Nothing)
    C_Font_Name = PropBag.ReadProperty("Font_Name", "΢���ź�")
    C_Font_Size = PropBag.ReadProperty("Font_Size", 10)
    C_Font_Bold = PropBag.ReadProperty("Font_Bold", False)
    C_Font_Italic = PropBag.ReadProperty("Font_Italic", False)
    C_Font_Underline = PropBag.ReadProperty("Font_Underline", False)
    C_Enabled = PropBag.ReadProperty("Enabled", True)
    C_Distance = PropBag.ReadProperty("Distance", 240)
    C_SelectedColor = PropBag.ReadProperty("SelectedColor", &HFFFFFF)
    C_Color_Selecter = PropBag.ReadProperty("Color_Selecter", &HFFFFFF)
    Refresh
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Color_Back", C_Color_Back, &HE1A400)
    Call PropBag.WriteProperty("Color_Text", C_Color_Text, &H0&)
    Call PropBag.WriteProperty("Picture", C_Picture, Nothing)
    Call PropBag.WriteProperty("Font_Name", C_Font_Name, "΢���ź�")
    Call PropBag.WriteProperty("Font_Size", C_Font_Size, 10)
    Call PropBag.WriteProperty("Font_Bold", C_Font_Bold, False)
    Call PropBag.WriteProperty("Font_Italic", C_Font_Italic, False)
    Call PropBag.WriteProperty("Font_Underline", C_Font_Underline, False)
    Call PropBag.WriteProperty("Enabled", C_Enabled, True)
    Call PropBag.WriteProperty("Distance", C_Distance, 240)
    Call PropBag.WriteProperty("SelectedColor", C_SelectedColor, &HFFFFFF)
    Call PropBag.WriteProperty("Color_Selecter", C_Color_Selecter, &HFFFFFF)
End Sub

